package com.nn.indicatorbar;

import java.util.List;

import com.example.indicatorbar.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MyAdapter extends BaseAdapter{
	private List<MyMenu> list;
	private Context context;

	public MyAdapter(List<MyMenu> list, Context context) {
		super();
		this.list = list;
		this.context = context;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return list.get(arg0);
	}

	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}
	private Holder holder;
	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
		// TODO Auto-generated method stub
		if(arg1==null){
			arg1=LayoutInflater.from(context).inflate(R.layout.indicator_item_view, null);
			holder=new Holder();
//			holder.ivImage=(ImageView) arg1.findViewById(R.id.item_image);
			holder.tvTitle=(TextView) arg1.findViewById(R.id.item_title);
			holder.tvCount=(TextView) arg1.findViewById(R.id.item_count);
			arg1.setTag(holder);
		}else{
			holder=(Holder) arg1.getTag();
		}
//		if(!list.get(arg0).isShowImage()){
//			holder.ivImage.setVisibility(View.GONE);
//		}else{
//			holder.ivImage.setVisibility(View.VISIBLE);
//			holder.ivImage.setImageResource(list.get(arg0).getImageRes());
//		}
		holder.tvTitle.setText(list.get(arg0).getTitle());
		if(!list.get(arg0).isShowCount()){
			holder.tvCount.setVisibility(View.GONE);
		}else{
			holder.tvCount.setVisibility(View.VISIBLE);
			holder.tvCount.setText(list.get(arg0).getCount()+"");
		}
		return arg1;
	}
	private class Holder{
		private ImageView ivImage;
		private TextView tvTitle;
		private TextView tvCount;
	}
}
