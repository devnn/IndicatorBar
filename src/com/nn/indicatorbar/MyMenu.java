package com.nn.indicatorbar;

public class MyMenu {
	private int index;
	private String title;
	private int ImageRes;
	private boolean showImage;
	private boolean showCount;
	private int count;
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getImageRes() {
		return ImageRes;
	}
	public void setImageRes(int imageRes) {
		ImageRes = imageRes;
	}
	public boolean isShowImage() {
		return showImage;
	}
	public void setShowImage(boolean showImage) {
		this.showImage = showImage;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public boolean isShowCount() {
		return showCount;
	}
	public void setShowCount(boolean showCount) {
		this.showCount = showCount;
	}

}
